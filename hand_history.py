from glob import iglob


class Hand:

    def __init__(self, hand_str):
        pass


def get_history_path():
    for file in iglob('*.eml'):
        pass
    return file


def read_history_file(file):
    with open(file, 'r') as history_file:
        hands = []
        current_hand = ''
        for line in history_file:
            if line.startswith('*' * 10):
                hands.append(clean_hand(current_hand))
                current_hand = line
            else:
                current_hand += line
    return hands[1:]


def clean_hand(hand):
    return (hand
            .replace('=20', '')
            .replace('skar96', 'Oskar'))


def read_hands():
    file = get_history_path()
    return read_history_file(file)


def parse_my_preflop_action(hand):
    pre_flop_start = hand.index('*** HOLE CARDS ***')
    pre_flop_end = hand.index('*** FLOP ***')
    pre_flop_str = hand[pre_flop_start: pre_flop_end]
    my_action = pre_flop_str[pre_flop_str.index('Oskar: '):]
    my_action = my_action[:my_action.index('\n')]
    my_action = my_action.replace('Oskar: ', '')
    return my_action


def filter_for_me(hands):
    hands = [hand for hand in hands if 'FLOP' in hand]
    return [hand for hand in hands
            if 'fold' not in parse_my_preflop_action(hand)]


def write_hands(hands):
    print(f'Writing {len(hands)} hands to file...')
    with open('hands.txt', 'w') as f:
        for hand in hands:
            f.write(hand)


if __name__ == '__main__':
    hands = read_hands()
    hands = filter_for_me(hands)
    write_hands(hands)
